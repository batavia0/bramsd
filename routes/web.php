<?php

use App\Http\Controllers\UserController;
use App\Http\Controllers\uang_kasController;
use App\Http\Controllers\anggotaController;
use App\Http\Controllers\pengeluaranController;
use App\Http\Controllers\laporanController;
use App\Http\Controllers\paymentController;
use App\Http\Controllers\ApiController;
use App\Models\m_uang_kas;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { 
    $data['contoh'] = DB::table('uang_kas')->sum('terbayar');
    $data['substract'] =  DB::table('pengeluaran')->sum('jumlah_pengeluaran');
    $data['countAnggota'] = DB::table('anggota')->count('id_anggota');
    $data['title'] = 'Welcome';
    return view('home',$data);
})->middleware('auth')->name('home');

// Route::get('home', [UserController::class, 'home']);

/*
|--------------------------------------------------------------------------
| Routes for Views
|--------------------------------------------------------------------------
*/
//Payment
// Route::post('/payment', [paymentController::class, 'payment_post'])->name('payment_post');
// Route::get('/paymentdetails', [paymentController::class, 'index'])->name('paymentdetails');
Route::get('/payment', [UserController::class, 'payment'])->name('payment')->middleware('auth');
Route::get('/payment/handle/{id_bulan_pembayaran}', [PaymentController::class, 'getpayment'])->name('getpayment');
Route::post('/payment/handle/{id_bulan_pembayaran}', [PaymentController::class, 'payment_post']);
//End Payment

//User Control
Route::get('/register', [UserController::class, 'register'])->name('register');
Route::get('/login', [UserController::class, 'login'])->name('login');
Route::get('/password', [UserController::class, 'password'])->name('password');
Route::get('/profile', [UserController::class,'profile'])->name('profile');
//END USER CONTROL
Route::get('/uang_kas',[UserController::class,'uang_kas'])->name('uang_kas');
Route::get('/detail_uang_kas/{id_bulan_pembayaran}',[uang_kasController::class,'showDetails'])->name('detail_uang_kas');
Route::get('/tambah_bulan',[uang_kasController::class,'tambahBulan'])->name('tambah_bulan');
Route::get('/tambah_anggota',[UserController::class,'tambah_anggota'])->name('tambah_anggota');
Route::get('/v_pengeluaran', [pengeluaranController::class,'pengeluaran'])->name('pengeluaran');
Route::get('/v_detailpengeluaran/{id_pengeluaran}', [pengeluaranController::class,'detail'])->name('detail_pengeluaran');
Route::get('/v_tambahpengeluaran', [pengeluaranController::class,'tambah_pengeluaran'])->name('tambah_pengeluaran')->middleware('auth');
Route::get('/anggota',[UserController::class,'anggota'])->name('anggota');  
/*
Route::get('/v_pengeluaran/edit/{id_pengeluaran}', [pengeluaranController::class,'ubah_pengeluaran']);
Route::post('/pengeluaran/update/{id_pengeluaran}', [pengeluaranController::class,'update']);
Route::get('/pengeluaran/delete/{id_pengeluaran}', [pengeluaranController::class,'hapus_pengeluaran']);
*/
Route::get('/riwayat', [UserController::class,'riwayatKasMasuk'])->name('riwayatpemasukkan');
Route::get('/riwayat_pengeluaran', [UserController::class,'riwayatKasKeluar'])->name('riwayatpengeluaran');
Route::get('/laporan', [laporanController::class,'laporan'])->name('laporan');
Route::post('/print_laporan_pemasukkan', [laporanController::class,'laporanPemasukkan'])->name('print.pemasukkan');
Route::get('/belajar', [UserController::class,'belajarphp'])->name('belajar');

/*
|--------------------------------------------------------------------------
| Routes for Actions
|--------------------------------------------------------------------------
*/
Route::post('/register', [UserController::class, 'register_action'])->name('register.action');
Route::post('/login', [UserController::class, 'login_action'])->name('login.action');
Route::post('/password', [UserController::class, 'password_action'])->name('password.action');
Route::put('/profile', [UserController::class,'profile_action'])->name('profile.action');
Route::put('/detail_uang_kas{id_uang_kas}',[uang_kasController::class,'uang_kas_action'])->name('update.uang_kas');
Route::get('/anggota{id_anggota}', [anggotaController::class, 'destroy'])->name('delete.anggota');
Route::post('/anggota{id_anggota}', [anggotaController::class,'update'])->name('update.anggota');
Route::post('/anggota', [anggotaController::class,'store'])->name('tambah.anggota');
Route::post('/detail_uang_kas/{id_bulan_pembayaran}', [uang_kasController::class, 'insert_anggota'])->name('insertanggota'); //PAUSE DUE TO INCOSCISTENCY
Route::get('/logout', [UserController::class, 'logout'])->name('logout');
Route::delete('/uang_kas{id_bulan_pembayaran}',[uang_kasController::class,'destroy'])->name('delete');
Route::put('uang_kas{id_uang_kas}',[uang_kasController::class,'update'])->name('update');
Route::post('/tambah_bulan',[uang_kasController::class,'action_bulan_pembayaran'])->name('action_bulan_pembayaran');
Route::post('/v_tambahpengeluaran', [pengeluaranController::class,'store'])->name('action.pengeluaran');
Route::get('/v_tambahpengeluaran/{id_pengeluaran}', [pengeluaranController::class,'destroy'])->name('delete.pengeluaran');
Route::post('/v_pengeluaran/{id_pengeluaran}', [pengeluaranController::class,'update'])->name('update.pengeluaran');
Route::get('/v_pengeluaran/{id_pengeluaran}', [pengeluaranController::class,'show'])->name('detail.pengeluaran');
Route::get('/laporan/print_pemasukkan', [laporanController::class,'laporanPemasukkan'])->name('x.pemasukkan');
Route::get('/laporan/print_pengeluaran', [laporanController::class,'laporanPengeluaran'])->name('print.pengeluaran');
/*
|--------------------------------------------------------------------------
| Routes for Actions Payment
|--------------------------------------------------------------------------
*/
Route::post('/payment-handler', [ApiController::class, 'payment_handler'])->name('payment-handler'); //Tangkap API JSON Midtrans

// Route::get('detail_uang_kas', function(){
// })->middleware('auth')->name('detail_uang_kas'); //Akses dibatasi sampai user login terlebih dahulu